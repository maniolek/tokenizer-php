<?php
/**
 * @author Mateusz Aniołek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerPhp\Tests;

include_once 'Mocks/ConnectorMock.php';

use TokenizerPhp\Tests\Mocks\Tokenizer\ConnectorMock;

class TokenizerTest extends \PHPUnit_Framework_TestCase
{

    public function setUp() { }

    public function configDataProvider()
    {
        return array(
            array('0', '1234567890', 'http://fake.response.url.com/authorize/')
        );
    }

    /**
     * @expectedException \TokenizerPhp\Tokenizer\Exception
     */
    public function testEmptyParams()
    {
        $tokenizer = new \TokenizerPhp\Tokenizer(array());
        throw new \Exception('Not this exception');
    }

    /**
     * @dataProvider configDataProvider
     */
    public function testConnector($id, $key, $url)
    {

        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => $key,
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $tokenizer->verifyConfig();

    }

    /**
     * @dataProvider configDataProvider
     * @expectedException \TokenizerPhp\Tokenizer\Exception\RedirectUrlNotGivenException
     */
    public function testCreateAuthEmptyRedirect($id, $key, $url)
    {

        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => $key,
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $tokenizer->createAuth('make.exception@email.com', null);
        throw new \Exception('Not this exception');

    }

    /**
     * @dataProvider configDataProvider
     * @expectedException \TokenizerPhp\Tokenizer\Exception\ResponseNotGivenException
     */
    public function testCreateAuthFail($id, $key, $url)
    {

        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => $key,
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $tokenizer->createAuth('make.exception@email.com', $url, false);
        throw new \Exception('Not this exception');

    }

    /**
     * @dataProvider configDataProvider
     */
    public function testCreateAuth($id, $key, $url)
    {

        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => $key,
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $tokenizer->createAuth('example@email.com', $url, false);
    }

    /**
     * @dataProvider configDataProvider
     */
    public function testVerifyAuthFail($id, $key, $url)
    {
        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => 'failKey',
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $this->assertFalse($tokenizer->verifyAuth($id));

    }

    /**
     * @dataProvider configDataProvider
     */
    public function testVerifyAuthAccepted($id, $key, $url)
    {
        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $id,
            'app_key' => 'acceptedKey',
        ));

        $tokenizer->setConnector(new ConnectorMock());
        $this->assertTrue($tokenizer->verifyAuth($id));

    }

}