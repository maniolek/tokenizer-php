<?php

namespace TokenizerPhp\Tests\Mocks\Tokenizer;

use TokenizerPhp\Tokenizer\Connector;
use TokenizerPhp\Tokenizer\Response;

class ConnectorMock extends Connector
{
    public function curlGet($name, $options = array())
    {
        $response_array = array();

        if($name == 'config') {
            $response_array = array(
                'name' => 'service_name',
                'domain' => 'service_domain',
                'max' => 'service_max_users',
                'server_time' => 'server_time',
                'icon' => array(
                    'icon1' => 'url_of_icon1',
                    'icon2' => 'url_of_icon2'
                )
            );
        }

        if($name == 'verify' and $options['app_key'] == 'acceptedKey') {
            $response_array = array('state' => 'accepted');
        }

        if($name == 'verify' and $options['app_key'] == 'failKey') {
            $response_array = array();
        }

        return Response::parse($name,
            json_encode($response_array)
        );

    }
    public function curlPost($name, array $options)
    {

        if($options['usr_email'] == 'make.exception@email.com') {
            return null;
        }

        if($options['usr_email'] == 'example@email.com') {
            return Response::parse($name,
                json_encode(array(
                    'id' => 'example_value',
                    'wait_url' => 'http://example.url.com/',
                ))
            );
        }

    }

}