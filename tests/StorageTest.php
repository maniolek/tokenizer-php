<?php
/**
 * @author Mateusz Aniołek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerPhp\Tests;

include_once 'Stub/FakeStorage.php';

class StorageTest extends \PHPUnit_Framework_TestCase
{
    private $fakeAppId = '0';
    private $fakeAppKey = '1234567890';
    private $fakeAppUrl = 'http://fake.response.url.com/authorize';

    public function setUp() { }

    public function testSessionStorageNotGiven()
    {
        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $this->fakeAppId,
            'app_key' => $this->fakeAppKey,
        ));
        // getSessionStorage for default should return php global session
        $this->assertInstanceOf('\TokenizerPhp\Tokenizer\Storage\GlobalSessionStorage', $tokenizer->getSessionStorage());

    }

    public function testSessionStorage()
    {
        $tokenizer = new \TokenizerPhp\Tokenizer(array(
            'app_id' => $this->fakeAppId,
            'app_key' => $this->fakeAppKey,
        ));
        $session = $tokenizer->getSessionStorage();
        $session->set('test', 123);

        $this->assertEquals(123, $session->get('test'));

        $this->assertEquals(null, $session->get('test_null'));

        $tokenizer->setSessionStorage(new \TokenizerPhp\Tests\Stub\FakeStorage);

        $session = $tokenizer->getSessionStorage();
        $session->set('test', 'test_value');

        $this->assertEquals('test_value', $session->get('test'));


    }

}